<Query Kind="Program" />

// CA_URL https://bakuaustralia.phocassoftware.com/Administration/CustomAction/Index/7
// CA_PARAMETERS .\TandaParameters.json

// CA_REMOVE // CA_ADD, CA_PARAMETERS, CA_REMOVE AND CA_URL are special comments used by the harness. 
// CA_REMOVE // Any lines with CA_ADD will have the '// CA_ADD ' portion removed and the rest of the line will remain.
// CA_REMOVE // A single line with CA_PARAMETERS will be used to determine an (optional) JSON file that contains definitions of parameters for the Custom Action.
// CA_REMOVE // Any lines with CA_REMOVE will be replaced with a blank line (so that line and column numbers are preserved in exception messages).
// CA_REMOVE // CA_URL should be the first line of the LINQPad script and will be used to determine the Phocas organisation and Custom Action ID to interact with.

ICustomActionsAPI API = default(ICustomActionsAPI); // CA_REMOVE
public void Main() { "Looks good.".Dump(); } // CA_REMOVE
DateTime currentTime = DateTime.Now; //CA_REMOVE

// Source: Will Ferguson 2019-11-26  8pm v1.0 
// CA_ADD Run();

static HttpClient httpClient = new HttpClient();

const string salesHeaderItemNameParameter = "salesHeaderItemName";
const string salesLineItemsItemNameParameter = "salesLineItemsItemName";

void Run()
{
var itemDefinitions = LoadItemDefinitions();
var sourceName = API.Parameter.Get("sourceName");


API.Sync.Sources.Create(sourceName);
API.Sync.Sources.Start(sourceName);


foreach (var itemDefinition in itemDefinitions)
{
var itemName = itemDefinition.Name == "Sales" ? API.Parameter.Get(salesHeaderItemNameParameter) : itemDefinition.Name;

try
{
if (!itemDefinition.IsEnabled)
{
continue;
}

var request = CreateRequestForItemDefinition(itemDefinition);

API.Sync.Items.Create(sourceName, itemName);
API.Sync.Items.Start(sourceName, itemName);

if (itemDefinition.Name == "Sales")
{
var apiDataResponse = ApiDataRequestHelper.GetDataFromApiRequest(request);

// SALES - ONLY LINE ITEMS
var sales = apiDataResponse.ResponseJArray.DeepClone();
var lineItemsOnly = new JArray();

foreach (var salesLineToken in sales.Children())
{
if (!(salesLineToken is JObject salesLine))
continue;

// Get the value of the id key from the Sale object
var saleID = salesLine.GetValue("id");
var saleDate = salesLine.GetValue("sale_date");
var saleUpdatedAt = salesLine.GetValue("updated_at");

if (salesLine.GetValue("line_items") == null)
continue;

// Get the value of the line_items key from the Sale object
var lineItemToken = salesLine.GetValue("line_items") as JArray;

// Iterate over each line_item value
foreach (var lineItem in lineItemToken)
{
if (!(lineItem is JObject jObject))
continue;

// Add the Sale's id, sale date, and last updated date to the line_item's record
jObject.Add(new JProperty("sale_id", saleID));
jObject.Add(new JProperty("sale_date", saleDate));
jObject.Add(new JProperty("sale_updated_at", saleUpdatedAt));
// Add the line_item to the lineItems JArray
lineItemsOnly.Add(jObject);
}
}

// SALES - WITHOUT LINE ITEMS
// Remove the line_items key and value from the salesWithoutLineItems array
var salesWithoutLineItems = apiDataResponse.ResponseJArray.DeepClone();
foreach (JObject salesLine in salesWithoutLineItems.Children())
{
salesLine.Property("line_items").Remove();
}

SyncSalesLineItems(sourceName, itemDefinition, request, lineItemsOnly);
SyncSalesWithoutLineItems(sourceName, itemDefinition, request, salesWithoutLineItems);
}
else if (itemDefinition.IncrementalProcessType == "ByKey")
{
API.Sync.Items.IncrementDataByKey(sourceName, itemName, request, itemDefinition.IncrementalDataColumnNames);
}
else
{
API.Sync.Items.ClearData(sourceName, itemName);
API.Sync.Items.AppendData(sourceName, itemName, request);
}
}
catch (Exception exception)
{
var innerExceptionMessage = exception.InnerException != null ? "\nInner exception: " + exception.InnerException.Message : "";
API.Sync.Sources.Log(sourceName, API.Sync.LogSeverities.ERROR, $"API request for item '{itemName}' failed.\nException: {exception}" + innerExceptionMessage);
API.Sync.Items.Fail(sourceName, itemName, exception.Message + innerExceptionMessage);
API.Sync.Sources.Fail(sourceName, exception.Message);
throw;
}

API.Sync.Items.End(sourceName, itemName, warningCount: 0);
}
API.Sync.Sources.Process(sourceName);
API.Sync.Sources.End(sourceName);
}

public void SyncSalesLineItems(string sourceName, ItemDefinition itemDefinition, ApiDataRequest request, JToken lineItemsOnly)
{
var lineItemsItemName = API.Parameter.Get(salesLineItemsItemNameParameter);
API.Sync.Items.Create(sourceName, lineItemsItemName);
API.Sync.Items.Start(sourceName, lineItemsItemName);

var lineItemsConverter = new Converter();
lineItemsConverter.ArrayValueHandling = request.ArrayValueHandling;
lineItemsConverter.ObjectValueHandling = request.ObjectValueHandling;
lineItemsConverter.ColumnNameDelimiter = request.ColumnNameDelimiter;

var lineItemsDataTable = new DataTable();
lineItemsConverter.Fill((JArray)lineItemsOnly, lineItemsDataTable);

API.Sync.Items.IncrementDataByKey(sourceName, lineItemsItemName, lineItemsDataTable, itemDefinition.IncrementalDataColumnNames);
API.Sync.Items.End(sourceName, lineItemsItemName, warningCount: 0);
}

public void SyncSalesWithoutLineItems(string sourceName, ItemDefinition itemDefinition, ApiDataRequest request, JToken salesWithoutLineItems)
{
var salesHeaderItemName = API.Parameter.Get(salesHeaderItemNameParameter);
var salesConverter = new Converter();
salesConverter.ArrayValueHandling = request.ArrayValueHandling;
salesConverter.ObjectValueHandling = request.ObjectValueHandling;
salesConverter.ColumnNameDelimiter = request.ColumnNameDelimiter;

var salesDataTable = new DataTable();
salesConverter.Fill((JArray)salesWithoutLineItems, salesDataTable);
API.Sync.Items.IncrementDataByKey(sourceName, salesHeaderItemName, salesDataTable, itemDefinition.IncrementalDataColumnNames);
}

public string GetHeaderAuthValue() => "Bearer " + API.Parameter.Get("personalToken");

public class ItemDefinition
{
	public string Name { get; set; }
	public bool IsEnabled { get; set; }

	public string PathToCollection { get; set; }
	public string RelativeUrl { get; set; }

	public string IncrementalProcessType { get; set; }
	public string LastModifiedDateColumnName { get; set; }
	public string LastModifiedDateColumnFormat { get; set; }
	public string DateFilterColumnName { get; set; }
	public List<string> IncrementalDataColumnNames { get; set; }

	public string ArrayValueHandling { get; set; }
	public string ArrayValueHandlingDelimiter { get; set; }
	public string ObjectValueHandling { get; set; }
	public string ColumnNameDelimiter { get; set; }
}

public class ApiDataResponse
{
	public string ResponseBody { get; set; }
	public JArray ResponseJArray { get; set; }
}

public ApiDataRequest CreateRequestForItemDefinition(ItemDefinition itemDefinition)
{
	var maxVersion = GetCurrentMaxVersion(itemDefinition);
	var url = UrlCombine(API.Parameter.Get("baseUrl"), itemDefinition.RelativeUrl);
	var authHeader = new Header { Name = "Authorization", Value = GetHeaderAuthValue() };

	Enum.TryParse<ArrayValueHandling>(itemDefinition.ArrayValueHandling, true, out ArrayValueHandling arrayValueHandling);
	Enum.TryParse<ObjectValueHandling>(itemDefinition.ObjectValueHandling, true, out ObjectValueHandling objectValueHandling);

	if (itemDefinition.IncrementalProcessType == "ByKey")
	{
		if (maxVersion != null)
		{
			url = url + $"?after={maxVersion}";
		}
	}
	else
	{
		url = url + "?page_size=10000";
	}

	var request = new ApiDataRequest
	{
		URL = url,
		PathToCollection = itemDefinition.PathToCollection,
		Headers = new List<IHeader> { authHeader },
		ArrayValueHandling = arrayValueHandling,
		ObjectValueHandling = objectValueHandling,
		ArrayValueHandlingDelimiter = itemDefinition.ArrayValueHandlingDelimiter,
		ColumnNameDelimiter = itemDefinition.ColumnNameDelimiter
	};

	return request;
}

public string GetCurrentMaxVersion(ItemDefinition itemDefinition)
{
	try
	{
		var columnName = QuoteIdentifier("version");
		var tableName = QuoteIdentifier($"{API.Parameter.Get("sourceName")}_{itemDefinition.Name}");

		var sql = $"SELECT MAX({columnName}) FROM [sync].{tableName}";

		using (var command = new SqlCommand(sql, API.SyncConnection))
		{
			var version = command.ExecuteScalar();
			return version.ToString();
		}
	}
	catch (Exception)
	{
		return null;
	}
}

string QuoteIdentifier(string identifier)
{
	using (var commandBuilder = new SqlCommandBuilder())
	{
		return commandBuilder.QuoteIdentifier(identifier);
	}
}

public string UrlCombine(string one, string two) => one?.TrimEnd('/') + "/" + two?.TrimStart('/');

IList<ItemDefinition> LoadItemDefinitions()
	=> JsonConvert.DeserializeObject<List<ItemDefinition>>(API.Parameter.Get("itemDefinitions"));

/* #region JSON to Datatable Converter */

public static class ClrTypes
{
	public static readonly Type Boolean = typeof(bool?);
	public static readonly Type DateTime = typeof(DateTime?);
	public static readonly Type Float = typeof(double?);
	public static readonly Type Guid = typeof(Guid?);
	public static readonly Type Int = typeof(long?);
	public static readonly Type Object = typeof(object);
	public static readonly Type String = typeof(string);
	public static readonly Type TimeSpan = typeof(TimeSpan?);
	public static readonly Type Uri = typeof(Uri);
}

public class Converter
{
	public ArrayValueHandling ArrayValueHandling { get; set; } = ArrayValueHandling.Ignore;

	public string ArrayValueHandlingDelimiter { get; set; } = ";";

	public string ColumnNameDelimiter { get; set; } = "_";

	public ObjectValueHandling ObjectValueHandling { get; set; } = ObjectValueHandling.Ignore;

	private IDataTableWrapper DataTable { get; set; }

	public void Fill(JArray jArray, DataTable dataTable)
		=> Fill(jArray, new DataTableWrapper(dataTable));

	public void Fill(JArray jArray, IDataTableWrapper dataTable)
	{
		DataTable = dataTable;

		if (jArray.Any(jToken => !(jToken is JObject)))
			throw new ArgumentException("Only array of objects is supported", nameof(jArray));

		using (CultureContext.AsPhocasStandardCulture())
		{
			foreach (JObject jObject in jArray)
			{
				DataTable.StartNewRow();
				ProcessObject(jObject);
			}
		}

		DataTable.ChangeObjectColumnsToString();
	}

	private static object GetValue(JValue jValue)
	{
		var clrType = JTokenTypeToClrType.GetClrType(jValue);

		var value = jValue.Value;

		if (clrType == ClrTypes.DateTime && (value as DateTime?) == new DateTime(1, 1, 1, 0, 0, 0))
			value = null;

		return value;
	}

	private string DelimiterSeparateValues(JArray jArray)
		=> String.Join(ArrayValueHandlingDelimiter, jArray.Values());

	private string GetArrayValueColumnName(int index, string parentColumnName)
		=> (parentColumnName == null ? "" : parentColumnName + ColumnNameDelimiter) + (index + 1);

	private string GetPropertyColumnName(JProperty property, string parentColumnName)
		=> (parentColumnName == null ? "" : parentColumnName + ColumnNameDelimiter) + property.Name;

	private void ProcessArray(JArray jArray, string parentColumnName)
	{
		switch (ArrayValueHandling)
		{
			case ArrayValueHandling.Ignore:
				return;

			case ArrayValueHandling.Split:
				foreach (var (index, value) in jArray.Select((value, index) => (index, value)))
					ProcessToken(value, GetArrayValueColumnName(index, parentColumnName));
				break;

			case ArrayValueHandling.DelimiterSeparated:
				ProcessToken(new JValue(DelimiterSeparateValues(jArray)), parentColumnName);
				break;

			case ArrayValueHandling.JSON:
				ProcessToken(new JValue(jArray?.ToString(Newtonsoft.Json.Formatting.Indented)), parentColumnName);
				return;

			default:
				return;
		}
	}

	private void ProcessObject(JObject jObject, string parentColumnName = null)
	{
		var isNestedObject = parentColumnName != null;

		if (isNestedObject)
			switch (ObjectValueHandling)
			{
				case ObjectValueHandling.Ignore:
					return;

				case ObjectValueHandling.Split:
					break;

				case ObjectValueHandling.JSON:
					ProcessToken(new JValue(jObject?.ToString(Newtonsoft.Json.Formatting.Indented)), parentColumnName);
					return;

				default:
					return;
			}

		foreach (var property in jObject.Properties())
			ProcessToken(property.Value, GetPropertyColumnName(property, parentColumnName));
	}

	private void ProcessToken(JToken jToken, string columnName)
	{
		switch (jToken)
		{
			case JArray jArray:
				ProcessArray(jArray, columnName);
				break;

			case JValue jValue:
				ProcessValue(jValue, columnName);
				break;

			case JObject jObject:
				ProcessObject(jObject, columnName);
				break;

			default:
				return;
		}
	}

	private void ProcessValue(JValue jValue, string columnName)
	{
		DataTable.AddOrUpdateColumn(columnName, JTokenTypeToClrType.GetClrType(jValue));
		DataTable.AddValueToCurrentRow(columnName, GetValue(jValue));
	}
}

public static class DataColumnReplacer
{
	public static DataColumn CopyColumn(DataColumn column, Type newType)
	{
		var isNullableType = Nullable.GetUnderlyingType(newType) != null;
		var columnType = Nullable.GetUnderlyingType(newType) ?? newType;

		var newColumn = new DataColumn(column.ColumnName, columnType)
		{
			AllowDBNull = isNullableType || !newType.IsValueType,
			Namespace = column.Namespace
		};

		return newColumn;
	}

	public static void ReplaceColumn(DataTable dataTable, DataColumn column, Type newType)
	{
		var values = Enumerable.Range(0, dataTable.Rows.Count).Select(i => dataTable.Rows[i]).Select(r => r[column]).ToList();

		var newColumn = CopyColumn(column, newType);

		var ordinal = column.Ordinal;
		dataTable.Columns.Remove(column);
		dataTable.Columns.Add(newColumn);
		newColumn.SetOrdinal(ordinal);

		if (newType == typeof(string))
		{
			using (CultureContext.AsPhocasStandardCulture())
			{
				for (var i = 0; i < dataTable.Rows.Count; i++)
					dataTable.Rows[i][newColumn] = values[i].ToString();
			}
		}
		else
		{
			for (var i = 0; i < dataTable.Rows.Count; i++)
				dataTable.Rows[i][newColumn] = values[i];
		}
	}
}

public class DataTableWrapper : IDataTableWrapper
{
	public DataTableWrapper(DataTable dataTable)
	{
		DataTable = dataTable;
	}

	private DataRow CurrentRow { get; set; }

	private DataTable DataTable { get; }

	public void AddColumn(string columnName, Type type)
	{
		var underlyingType = Nullable.GetUnderlyingType(type) ?? type;

		var newColumn = new DataColumn(columnName, underlyingType)
		{
			AllowDBNull = true
		};

		DataTable.Columns.Add(newColumn);
	}

	public void AddOrUpdateColumn(string columnName, Type type)
	{
		var existingColumn = DataTable.Columns[columnName];

		if (existingColumn == null)
		{
			AddColumn(columnName, type);
			return;
		}

		var existingType = GetColumnDataType(existingColumn);

		var newType = TypeConversions.GetLowestCommonLosslessConvertibleType(type, existingType);

		if (newType == existingType)
			return;

		DataColumnReplacer.ReplaceColumn(DataTable, existingColumn, newType);
	}

	public void AddValueToCurrentRow(string columnName, object value)
		=> CurrentRow[columnName] = value ?? DBNull.Value;

	public void ChangeObjectColumnsToString()
	{
		var columns = Enumerable.Range(0, DataTable.Columns.Count).Select(i => DataTable.Columns[i]).ToList();

		foreach (var column in columns.Where(c => c.DataType == ClrTypes.Object).ToList())
			AddOrUpdateColumn(column.ColumnName, ClrTypes.String);
	}

	public void StartNewRow()
	{
		CurrentRow = DataTable.NewRow();
		DataTable.Rows.Add(CurrentRow);
	}

	private static Type GetColumnDataType(DataColumn column)
	{
		var type = column.DataType;
		return type.IsValueType ? typeof(Nullable<>).MakeGenericType(type) : type;
	}
}

public interface IDataTableWrapper
{
	void AddColumn(string columnName, Type type);

	void AddOrUpdateColumn(string columnName, Type type);

	void AddValueToCurrentRow(string columnName, object value);

	void ChangeObjectColumnsToString();

	void StartNewRow();
}

public static class JTokenTypeToClrType
{
	private static readonly Dictionary<JTokenType, Type> s_jTokenTypeToClrType = new Dictionary<JTokenType, Type>
		{
			{JTokenType.Boolean, ClrTypes.Boolean},
			{JTokenType.Bytes, ClrTypes.String}, // included for completeness, doesn't get generated by deserializer
            {JTokenType.Date, ClrTypes.DateTime},
			{JTokenType.Float, ClrTypes.Float},
			{JTokenType.Guid, ClrTypes.Guid}, // included for completeness, doesn't get generated by deserializer
            {JTokenType.Integer, ClrTypes.Int},
			{JTokenType.Null, ClrTypes.Object},
			{JTokenType.String, ClrTypes.String},
			{JTokenType.TimeSpan, ClrTypes.TimeSpan}, // included for completeness, doesn't get generated by deserializer
            {JTokenType.Undefined, ClrTypes.Object},
			{JTokenType.Uri, ClrTypes.Uri} // included for completeness, doesn't get generated by deserializer
        };

	public static Type GetClrType(JValue jValue)
		=> s_jTokenTypeToClrType[jValue.Type];
}

public static class TypeConversions
{
	private static readonly Dictionary<Type, List<Type>> s_canLosslessConvertDictionary = new Dictionary<Type, List<Type>>
		{
			{ClrTypes.Boolean, new List<Type> { ClrTypes.Int, ClrTypes.Float, ClrTypes.String }},
			{ClrTypes.DateTime, new List<Type> { ClrTypes.String } },
			{ClrTypes.Float, new List<Type>{ ClrTypes.String } },
			{ClrTypes.Guid, new List<Type>{ ClrTypes.String } },
			{ClrTypes.Int, new List<Type>{ ClrTypes.Float, ClrTypes.String } },
			{ClrTypes.String, new List<Type>() },
			{ClrTypes.TimeSpan, new List<Type>{ ClrTypes.String } },
			{ClrTypes.Uri, new List<Type>{ ClrTypes.String } }
		};

	public static Type GetLowestCommonLosslessConvertibleType(Type type1, Type type2)
	{
		if (type1 == ClrTypes.Object)
			return type2;
		if (type2 == ClrTypes.Object)
			return type1;

		if (type1 == type2)
			return type1;
		else if (CanLosslessConvert(type1, type2))
			return type2;
		else if (CanLosslessConvert(type2, type1))
			return type1;
		else if (s_canLosslessConvertDictionary.ContainsKey(type1) && s_canLosslessConvertDictionary.ContainsKey(type2))
			return s_canLosslessConvertDictionary[type1].Intersect(s_canLosslessConvertDictionary[type2]).FirstOrDefault() ?? ClrTypes.String;
		else
			return ClrTypes.String;
	}

	private static bool CanLosslessConvert(Type fromType, Type toType)
		=> s_canLosslessConvertDictionary.ContainsKey(fromType) && s_canLosslessConvertDictionary[fromType].Contains(toType);
}

public class CultureContext : IDisposable
{
	public const string DEFAULT_CULTURE = "en-GB";
	public const string ISO_8601_DATE_FORMAT = "yyyy-MM-dd";
	public const string ISO_8601_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private System.Globalization.CultureInfo _originalCulture;
	private System.Globalization.CultureInfo _originalUICulture;

	private CultureContext(string culture)
	{
		CaptureAndSetCulture(System.Globalization.CultureInfo.GetCultureInfo(culture));
	}

	private CultureContext(System.Globalization.CultureInfo cultureToSet)
	{
		CaptureAndSetCulture(cultureToSet);
	}

	public static CultureContext AsCulture(string culture)
		=> new CultureContext(culture);

	public static CultureContext AsPhocasStandardCulture()
	{
		var newCulture = (System.Globalization.CultureInfo)System.Globalization.CultureInfo.GetCultureInfo(DEFAULT_CULTURE).Clone();
		newCulture.DateTimeFormat.ShortDatePattern = ISO_8601_DATE_FORMAT;
		newCulture.DateTimeFormat.LongDatePattern = ISO_8601_DATE_TIME_FORMAT;
		return new CultureContext(newCulture);
	}

	public static string ObjectToStandardString(object obj)
	{
		if (obj == null)
			return String.Empty;

		if (obj is double dbl)
			return dbl.ToString("0.######");

		return obj.ToString();
	}

	public void Dispose()
	{
		System.Globalization.CultureInfo.CurrentCulture = _originalCulture;
		System.Globalization.CultureInfo.CurrentUICulture = _originalUICulture;
	}

	private void CaptureAndSetCulture(System.Globalization.CultureInfo cultureToSet)
	{
		_originalCulture = System.Globalization.CultureInfo.CurrentCulture;
		_originalUICulture = System.Globalization.CultureInfo.CurrentUICulture;

		System.Globalization.CultureInfo.CurrentCulture = cultureToSet;
		System.Globalization.CultureInfo.CurrentUICulture = cultureToSet;
	}
}

/* #endregion JSON to Datatable Converter */

public static class ApiDataRequestHelper
{
	public static ApiDataResponse GetDataFromApiRequest(ApiDataRequest apiDataRequest)
	{
		const string IsArrayPattern = @"^[\s]*\[";
		var apiDataResponse = new ApiDataResponse();

		apiDataResponse.ResponseBody = MakeApiRequestAsync(apiDataRequest).GetAwaiter().GetResult();

		var isArray = System.Text.RegularExpressions.Regex.Match(apiDataResponse.ResponseBody, IsArrayPattern);

		var requestedData = isArray.Success ? (JToken)JArray.Parse(apiDataResponse.ResponseBody) : JObject.Parse(apiDataResponse.ResponseBody);

		if (apiDataRequest.PathToCollection != null)
		{
			requestedData = requestedData.SelectToken(apiDataRequest.PathToCollection);

			if (requestedData == null)
				throw new Exception($"No data found at path '{apiDataRequest.PathToCollection}'.");
		}

		apiDataResponse.ResponseJArray = (JArray)requestedData;

		return apiDataResponse;
	}

	private static async Task<string> MakeApiRequestAsync(ApiDataRequest apiDataRequest)
	{
		using (var request = new HttpRequestMessage(System.Net.Http.HttpMethod.Get, new Uri(apiDataRequest.URL)))
		{
			ProcessRequestHeaders(request, apiDataRequest);

			using (var response = httpClient.SendAsync(request).Result)
			{
				response.EnsureSuccessStatusCode();
				return await response.Content.ReadAsStringAsync();
			}
		}
	}

	private static void ProcessRequestHeaders(HttpRequestMessage request, ApiDataRequest apiDataRequest)
	{
		request.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

		foreach (var header in apiDataRequest.Headers)
			request.Headers.Add(header.Name, header.Value);
	}

}